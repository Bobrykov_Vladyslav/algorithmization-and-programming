#include "functions.h"

extern CARRACE* race;
extern int size;

void StructureSorting(CARRACE* sort, int size)
{
    int increment = 3;    //���������� ������ ����������
    while (increment > 0)  //���� ���� ������
    {
        for (int i = 0; i < size; i++)  //��� ��� �������� ������
        {
            int j = i;          //������� ������ �� �������
            CARRACE temp = sort[i];
            // ����������� ���� ������� ������, �������� �� j-���
            // �� �������� ��������
            while ((j >= increment) && (sort[j - increment].engineCapacity < temp.engineCapacity))
            {  //���� ��������� ������� ����� ���������
                sort[j] = sort[j - increment]; //��������� ���� �� ������� �������
                j = j - increment;  //���������� �� ���������� ���������� ���������
            }
            sort[j] = temp; //�� �������� ���� ������� ���������� �������
        }
        if (increment > 1)      //ĳ���� ������ �� 2
            increment = increment / 2;
        else if (increment == 1)   //�������� ������ ���������
            break;  //�������� � �����
    }
}