#include "functions.h"

static int size_;

int SizeOfArray()
{
    size_ = 1;
    printf("Enter the amount of structure elements: ");
    do
    {
        if (size_ <= 0)
            printf("Enter the number bigger then 0: ");
        scanf_s("%d", &size_);
    } while (size_ <= 0);
    getchar();
    return size_;
}