#include "functions.h"

extern CARRACE* race;
extern int size;

int main()
{
    size = SizeOfArray();
    race = (CARRACE*)malloc(sizeof(CARRACE) * size);
    Procces(race, 'F', size);
    Procces(race, 'P', size);
    Procces(race, 'S', size);
    Procces(race, 'P', size);
    free(race);
    return 0;
}