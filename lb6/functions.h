#include <stdio.h>
#include <stdlib.h>

typedef struct carRace
{
    char brand[25];
    int manufactureYear;
    int engineCapacity;
    char owner[40];
}CARRACE;

CARRACE* race;
int size;

int SizeOfArray();
void Procces(CARRACE* object, char action, int size);
void PrintTable(CARRACE* print, int size);
void StructureFilling(CARRACE* filling, int size);
void StructureSorting(CARRACE* sort, int size);