#include "functions.h"

extern CARRACE* race;
extern int size;

void Procces(CARRACE* object, char action, int size)
{
    switch (action)
    {
    case 'F':
        StructureFilling(object, size);
        return;
    case 'S':
        StructureSorting(object, size);
        return;
    case 'P':
        PrintTable(object, size);
        return;
    }
}