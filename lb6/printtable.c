#include "functions.h"

extern CARRACE* race;
extern int size;

void PrintTable(CARRACE* print, int size)
{
    printf("\n\n%-40s%-25s%-20s%-20s", "Owner", "Car", "Manufacture Year", "Engine's capacity");
    for (CARRACE* ptr = print; ptr < print + size; ptr++)
    {
        printf("\n%-40s%-25s%-20d%-20d", ptr->owner, ptr->brand, ptr->manufactureYear, ptr->engineCapacity);
    }
}