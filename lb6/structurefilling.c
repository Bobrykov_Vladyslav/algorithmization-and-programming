#include "functions.h"

extern CARRACE* race;
extern int size;

void StructureFilling(CARRACE* filling, int size)
{
    for (CARRACE* ptr = filling; ptr < filling + size; ptr++)
    {
        printf("\nEnter name of the car owner: ");
        gets(ptr->owner);
        printf("Enter the brandmark of the car: ");
        gets(ptr->brand);
        printf("Enter car manufacture year: ");
        scanf_s("%d", &ptr->manufactureYear);
        printf("Enter capacity of engine: ");
        scanf_s("%d", &ptr->engineCapacity);
        getchar();
    }
}