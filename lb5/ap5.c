#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

static unsigned char months[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
static const char* months_names[] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
};

typedef unsigned int UI;

void print_date(UI, UI);
void first_case_print(UI, UI, ...);
void second_case_print(UI, UI, ...);
void third_case_print(UI, UI, ...);

int main()
{
    first_case_print(4, 1998, 32, 48, 23);
    first_case_print(5, 2004, 78, 10, 232, 171);
    UI var;
    printf("Enter one of many parameters... : ");
    scanf_s("%u", &var);
    first_case_print(9, 2016, 56, 212, 300, 129, var, 198.9, "lol", 'u');
    second_case_print(2019, 'i', 23, 'i', 72);
    second_case_print(2016, 'i', 78, 'i', 82, ';', 98);
    third_case_print(9, 1998, 32, 48, 23, 65, 69, 321, 73, 19);
    return 0;
}

void print_date(UI day_number, UI year)
{
    if (day_number > 356) {
        printf("Incorrect day entered!\n");
        return;
    }
    if (!(year % 4) && year % 100 || !(year % 400)) {
        months[1] = 29;
    }
    else if (day_number == 356) {
        printf("Sorry) This year has only 355 days)\n");
        return;
    }
    unsigned char month;
    UI day_number_tmp = day_number;
    for (month = 0; months[month] < day_number_tmp; day_number_tmp -= months[month++]);
    printf("%-7u%-11s%-7u\n", day_number, months_names[month], day_number_tmp);
}

void first_case_print(UI amount, UI year, ...)
{
    UI *par_ptr = &amount;
    printf("\n%-7s%-7s%-7s", "Day", "Month", "Day of Month\n");
    for (par_ptr += 2; amount - 1 > 0; amount--) {
        print_date(*(par_ptr++), year);
    }
}

void second_case_print(UI year, UI par, ...)
{
    printf("\n%-7s%-7s%-7s", "Day", "Month", "Day of Month\n");
    for (UI* ptr_par = &par; *ptr_par == 'i'; ptr_par++) {
        print_date(*++ptr_par, year);
    }
}

void third_case_print(UI amount, UI year, ...)
{
    UI* par_ptr = &amount + 1;
    va_list va_ptr; 
    va_start(va_ptr, *par_ptr);
    printf("\n%-7s%-7s%-7s", "Day", "Month", "Day of Month\n");
    for (par_ptr += 2; amount - 1 > 0; amount--) {
        print_date(va_arg(va_ptr, UI), year);
    }
    va_end(va_ptr);
}